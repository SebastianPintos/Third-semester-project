
public class Rectangulo {

	
	private int ancho;
	private int largo;
	
	public Rectangulo(int a,int l) {
		this.ancho = a;
		this.largo = l;
	}
	
	public int getAncho() {
		return ancho;
	}
	
	public int getLargo() {
		return largo;
	}
	
	
}
