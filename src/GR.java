import java.util.*;
public class GR {
	
	private int turno;
	private String ganador = "";
	private int largoTablero;
	private int anchoTablero;
	private char[][] tablero;
	private int puntosJ1;
	private int puntosJ2;
	private int[] libres1 = new int[2];
	private int[] libres2 = new int[2];
	
	
	public GR(int a,int l) {
		this.anchoTablero = a;
		this.largoTablero = l;
		tablero = new char[anchoTablero][largoTablero];
		for(int i = 0;i< anchoTablero;i++) {
			for(int j = 0; j<largoTablero;j++)
				tablero[i][j] = ' ';
		}
		turno  = 1;
		puntosJ1=0;
		puntosJ2=0;
	}
	
	
	@Override
	public String toString() {
		StringBuilder tab = new StringBuilder();
		for(int i = 0;i< anchoTablero;i++) {
			for(int j = 0; j < largoTablero; j++) {
				tab.append('[');
				tab.append(tablero[i][j]);
				tab.append(']');
			}
			tab.append("\n");
		}
		tab.append("\nPuntos jugador 1: ");
		tab.append(puntosJ1);
		tab.append("     Puntos jugador 2: ");
		tab.append(puntosJ2);
		tab.append("\n turno: ");
		tab.append(turno);
		return tab.toString();
	}


	public String jugar() {
		
		Random a = new Random();
		Random l = new Random();
		int ancho = a.nextInt(anchoTablero/3)+2;
		int largo = l.nextInt(largoTablero/3)+2;
		if(turno == 1 || turno == 2) {
			primerTurno(ancho,largo);
			return ganador;
		}
		
		//System.out.println(libres2[0]);
		//System.out.println(libres2[1]);
		System.out.println("rec actual "+ancho);
		System.out.println("rec actual "+largo);
		
		System.out.println("rec anterior "+libres1[0]);
		System.out.println("rec anterior "+libres1[1]);
		if(turno % 2 == 1) {
		while(verificacion(libres1[0],libres1[1])) {
		
			
			Rectangulo rec = new Rectangulo(ancho,largo);
			
			Random xRandom = new Random();
			Random yRandom = new Random();
			int x = libres1[0]-xRandom.nextInt(libres1[0]);
			int y = libres1[1];
			libres1[0] = x;
			libres1[1] = y;
			
			
			int xLibre = libres1[0];
			int yLibre = libres1[1];
			
			for(int i = xLibre; i < xLibre+rec.getAncho();i++) {
				for(int j = yLibre; j < yLibre+rec.getLargo(); j++ ) {
					
					tablero[i][j] = 'X';
					if(xLibre==i && yLibre==j)
						tablero[i][j] = (char)(turno+'0');
				}
			}
			
			puntosJ1 += (rec.getAncho()*rec.getLargo())/2;
			
			turno++;
			return ganador;
		}
		
		}
		else if(turno%2 == 0) {
			Rectangulo rec = new Rectangulo(ancho,largo);
			int xLibre = 0;
			int yLibre = 0;
			for(int i = rec.getAncho(); i > 0;i--) {
				for(int j = rec.getLargo(); j > 0; j-- ) {
					if(xLibre==i && yLibre==j)
						tablero[i][j] = (char)turno;
					tablero[anchoTablero-i][largoTablero-j] = 'O';
				}
			}
			puntosJ1 += (rec.getAncho()*rec.getLargo())/2;
			turno++;
		}
		return ganador;
	}
	
	private void primerTurno(int a,int l) {
		if(turno == 1 ) {
			Random xRandom = new Random();
			Random yRandom = new Random();
			System.out.println("a: "+a);
			int x = a-(xRandom.nextInt(a)-1);
			int y = l;
			libres1[0] = x;
			libres1[1] = y;
			Rectangulo rec = new Rectangulo(a,l);
			for(int i = 0; i < rec.getAncho();i++) {
				for(int j = 0; j < rec.getLargo(); j++ ) {
					if(i==0 && j==0)
						tablero[i][j] = (char)(turno+'0');
					else
						tablero[i][j] = 'X';
				}
			}
			puntosJ1 += (rec.getAncho()*rec.getLargo());
			turno++;
		}
		else if(turno == 2) {
			Random xRandom = new Random();
			Random yRandom = new Random();
			int x = a-xRandom.nextInt(a);
			int y = l-yRandom.nextInt(l);
			libres2[0] = x;
			libres2[1] = y;
			Rectangulo rec = new Rectangulo(a,l);
			for(int i = rec.getAncho(); i > 0;i--) {
				for(int j = rec.getLargo(); j > 0; j-- ) {
					if(i==rec.getAncho() && j==rec.getLargo())
						tablero[anchoTablero-i][largoTablero-j] = (char)(turno+'0');
					else
						tablero[anchoTablero-i][largoTablero-j] = 'O';
				}
			}
			puntosJ2 += (rec.getAncho()*rec.getLargo());
			turno++;
		}
		
	}
	
	private boolean verificacion(int a,int l) {
		boolean acu1 = true;
		for(int i = 0; i > a; i++) {
			for(int j = 0; j > l; j++ ) {
				acu1 = acu1 && tablero[anchoTablero+i][largoTablero+j] == ' ';
				if(!acu1)
					return false;
			}
		}
		return true;
	}
}
